# README

This is my second Career Foundry Rails App. It is an article/ blog site.

## App Information

App Name: cfblog (Abroad Blog)

Created: June 2016

Updated: September 2020; September 2022; January 2024

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/cfblog)

Production: [Link](https://abroad-blog.mctighecw.site)

## Run App

### Locally

Get app info:

```sh
$ rails about
```

Update gems, if necessary:

```sh
$ bundle update
```

Install gems:

```sh
$ bundle install
```

Start dev server:

```sh
$ bin/rails server
```

### With Docker

1. Fill out `.env` file values

2. Build Docker image and start container

```sh
$ docker-compose up -d --build
```

## Notes

- Ruby version 2.6.5 (project started with 2.3.1)
- Rails version 6.0.3 (project started with 4.2.6, then upgraded to 5.0.0)
- For practice, I built everything manually in this app: no scaffold; no Devise or CanCanCan gems used
- Originally deployed on [Heroku](https://www.heroku.com)

Last updated: 2025-03-08
