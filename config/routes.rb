Rails.application.routes.draw do

  resources :users
  resources :articles do
    resources :comments
  end

  root 'articles#home'

  get '/home', to: 'articles#home'
  get '/contributors', to: 'users#index'
  get '/search_results', to: 'articles#search_results'

  get '/sign_up', to: 'users#new'

  get '/my_profile', to: 'users#my_profile'
  get '/edit_profile', to: 'users#edit'
  post '/edit_profile', to: 'users#edit'

  get '/log_in', to: 'sessions#new'
  post 'log_in', to: 'sessions#create'
  
  get '/log_out', to: 'sessions#destroy'

  get '/new_article', to: 'articles#new'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end