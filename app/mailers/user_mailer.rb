class UserMailer < ApplicationMailer
  default from: "mctighecw@gmail.com"

  # Welcome message sent at user registration
  def welcome(user)
    @appname = "Abroad Blog"
    mail( :to => user.email,
          :subject => "Welcome to #{@appname}!")
  end
end