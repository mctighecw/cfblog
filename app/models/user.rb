class User < ActiveRecord::Base
  has_secure_password
  
  has_many :articles
  has_many :comments

  validates :username, uniqueness: true, length: { in: 4..12 }, on: :create
  validates :first_name, :last_name, presence: true
  validates :email, presence: true, uniqueness: true, on: :create
  validates :password, confirmation: true, length: { in: 6..20 }, on: :create
  validates :password_confirmation, presence: true, on: :create

  mount_uploader :picture, PictureUploader

  extend FriendlyId
  friendly_id :username, use: :slugged

  def admin?
    self.admin == true
  end

end