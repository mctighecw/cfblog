class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :article

  validates :body, length: {
    minimum: 2,
    maximum: 30,
    too_short: "Please write a little more",
    too_long: "Please keep comment under 30 characters" }

end