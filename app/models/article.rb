class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments

  validates :title, length: {
    minimum: 5,
    maximum: 20,
    too_short: "Please lengthen title",
    too_long: "Please shorten title" }
  validates :body, length: {
    minimum: 50,
    maximum: 5000,
    too_short: "Please write a little bit more (at least 50 characters)",
    too_long: "Please keep article below 5000 characters" }

  extend FriendlyId
  friendly_id :title, use: :slugged

end