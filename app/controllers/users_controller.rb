class UsersController < ApplicationController
 def index
    @users = User.all
  end

  def show
    @user = User.friendly.find(params[:id])
  end

  def my_profile
    @user = current_user
    @articles = @user.articles
  end

  def new
    @user = User.new
  end

  def create 
    @user = User.new(user_params)
    if @user.save 
      session[:user_id] = @user.id
      UserMailer.welcome(@user).deliver_now
      redirect_to '/', notice: 'Sign up was successful.'
    else 
      render 'new'
    end 
  end

  def edit
    if session[:user_id] && current_user
      @user = current_user
      @user.picture = params[:picture]
    else
      redirect_to '/', alert: 'You are not allowed to do this.'
    end
  end

  def update
    @user = current_user
    @user.picture = params[:picture]
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'Profile has been updated' }
      else
        format.html { render 'edit' }
      end
    end
  end

  def destroy
    if session[:user_id] && current_user
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User deleted' }
      end
    else
      redirect_to '/', alert: 'You are not allowed to do this.'
    end
  end

  private
  def user_params
    params.require(:user).permit(:email, :first_name, :last_name, :username, :home_country, :current_country, :languages, :description, :password, :password_confirmation, :picture, :remove_picture, :remote_picture_url)
  end

end