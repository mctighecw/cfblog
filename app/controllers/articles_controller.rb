class ArticlesController < ApplicationController
  def home
    @articles = Article.limit(3).order("created_at DESC")
  end

  def index
    @articles = Article.all.order("created_at DESC")
  end

  def show
    @article = Article.friendly.find(params[:id])
    @comments = @article.comments.order("created_at DESC")
  end

  def search_results
    if params[:q]
      search_term = params[:q]
      if Rails.env.production?
        @articles = Article.where("title ilike :search or body ilike :search", {search: "%#{search_term}%"})
      else
        @articles = Article.where("title like :search or body like :search", {search: "%#{search_term}%"})
      end
    end
  end

  def new
    if session[:user_id]
      @article = Article.new
    else
      redirect_to '/', alert: 'Please sign up or log in.'
    end
  end

  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id
    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article has been created.' }
      else
        format.html { render 'new' }
      end
    end
  end

  def edit
    @article = Article.friendly.find(params[:id])
    if session[:user_id] && (current_user.admin? || current_user.id == @article.user_id)
      @article = Article.friendly.find(params[:id])
    else
      redirect_to '/', alert: 'You are not allowed to do this.'
    end
  end

  def update
    @article = Article.friendly.find(params[:id])
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article has been updated.' }
      else
        format.html { render 'edit' }
      end
    end
  end

  def destroy
    @article = Article.friendly.find(params[:id])
    if session[:user_id] && (current_user.admin? || current_user.id == @article.user_id)
      @article = Article.friendly.find(params[:id])
      @article.destroy
      redirect_to articles_path
    else
      redirect_to '/', alert: 'You are not allowed to do this.'
    end
  end

  def article_params
    params.require(:article).permit(:title, :body, :user_id)
  end

end