class CommentsController < ApplicationController
  def create
    @article = Article.friendly.find(params[:article_id])
    if session[:user_id] && current_user
      @comment = @article.comments.new(comment_params)
      @comment.user = current_user
      @comment.save
      redirect_to article_path(@article)
    else
      redirect_to '/', alert: 'You are not allowed to do this.'
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    if session[:user_id] && (current_user.admin? || current_user == @comment.user)
      article = @comment.article
      @comment.destroy
      redirect_to article
    else
      redirect_to '/', alert: 'You are not allowed to do this.'
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:user_id, :body)
  end

end