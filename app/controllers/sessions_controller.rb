class SessionsController < ApplicationController
  def new
  end
  
  def create
    @user = User.find_by_email(params[:session][:email])
    if @user && @user.authenticate(params[:session][:password])
      session[:user_id] = @user.id
      redirect_to '/', notice: 'Successfully logged in.'
    else
      redirect_to '/log_in', alert: 'Please make sure you enter your correct email and password.'
    end
  end
  
  def destroy 
    session[:user_id] = nil
    reset_session   # Added to protect from session fixation
    redirect_to '/', notice: 'Successfully logged out.'
  end

end