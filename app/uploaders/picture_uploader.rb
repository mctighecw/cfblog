class PictureUploader < CarrierWave::Uploader::Base
  attr_accessor :picture, :remove_picture
  # include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Image storage

  # Storage of uploaded images - production or development
  if Rails.env.production?
    storage :fog
  else
    storage :file
  end

  # Image storage directory for development
  def store_dir
    "uploads/pictures"
  end

  # Rename uploaded file
  def filename
    "#{model.username}.jpg"
  end

  # Process files as they are uploaded

  # process scale: [400, 400]
  # process resize_to_fill: [300, 300]
  process convert: 'jpg'
  process resize_to_fit: [350, 350]

  # Create thumbnail of uploaded image
  version :thumb do
    process convert: 'jpg'
    process resize_to_fill: [150,150]
  end

  # White list of extensions which are allowed to be uploaded
  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def content_type_blacklist
    %w(text/json application/json)
  end

  def content_type_blacklist
    %w(iso)
  end

  def content_type_whitelist
    /image\//
  end

end