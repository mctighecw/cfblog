// Load Turbolinks
$(document).on("turbolinks:load", function(){

  // Fade out alerts
  $(".alert, .notice").delay(2000).fadeOut(800);

});