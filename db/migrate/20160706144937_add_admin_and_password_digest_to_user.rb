class AddAdminAndPasswordDigestToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :admin, :boolean, default: false, null: false
    add_column :users, :password_digest, :string
  end
end
