class CreateUsers < ActiveRecord::Migration[4.2]
  def change
    create_table :users do |t|
      t.string :email
      t.string :username
      t.string :first_name
      t.string :last_name
      t.string :home_country
      t.string :current_country
      t.string :languages
      t.string :description
      t.string :password

      t.timestamps null: false
    end
  end
end
