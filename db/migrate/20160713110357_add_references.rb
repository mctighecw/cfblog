class AddReferences < ActiveRecord::Migration[4.2]
  def change
    add_reference :articles, :user, index: true, foreign_key: true
    add_reference :comments, :user, index: true, foreign_key: true
    add_reference :comments, :article, index: true, foreign_key: true
  end
end
