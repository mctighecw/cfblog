# syntax = docker/dockerfile:experimental

# 1. Base image
ARG RUBY_VERSION=2.6.5
ARG VARIANT=jemalloc-slim
FROM quay.io/evl.ms/fullstaq-ruby:${RUBY_VERSION}-${VARIANT} as base

# Set up environment
ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true
ENV SECRET_KEY_BASE 1
ENV BUNDLE_WITHOUT development:test
ENV BUNDLE_PATH vendor/bundle

# Directories
RUN mkdir /app
WORKDIR /app
RUN mkdir -p tmp/pids


# 2. Install packages only needed at build time
FROM base as build_deps

ENV BUILD_PACKAGES "git build-essential libpq-dev wget curl gzip xz-utils"

RUN --mount=type=cache,id=dev-apt-cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,id=dev-apt-lib,sharing=locked,target=/var/lib/apt \
    apt-get update -qq && \
    apt-get install --no-install-recommends -y ${BUILD_PACKAGES} \
    && rm -rf /var/lib/apt/lists /var/cache/apt/archives


# 3. Ruby gems
FROM build_deps as gems

ARG BUNDLER_VERSION=2.1.4

# Install bundler and gems
RUN gem install -N bundler -v ${BUNDLER_VERSION}

COPY Gemfile* ./
RUN bundle install && rm -rf vendor/bundle/ruby/*/cache


# 4. Final image
FROM base

# Install deployment packages
ENV DEPLOY_PACKAGES "postgresql-client file nano nodejs"

RUN --mount=type=cache,id=prod-apt-cache,sharing=locked,target=/var/cache/apt \
    --mount=type=cache,id=prod-apt-lib,sharing=locked,target=/var/lib/apt \
    apt-get update -qq && \
    apt-get install --no-install-recommends -y \
    ${DEPLOY_PACKAGES} \
    && rm -rf /var/lib/apt/lists /var/cache/apt/archives

COPY --from=gems /app /app
COPY --from=gems /usr/lib/fullstaq-ruby/versions /usr/lib/fullstaq-ruby/versions
COPY --from=gems /usr/local/bundle /usr/local/bundle

COPY . .

# Adjust binstubs to run on Linux and set current working directory
RUN chmod +x /app/bin/* && \
    sed -i 's/ruby.exe/ruby/' /app/bin/* && \
    sed -i '/^#!/aDir.chdir File.expand_path("..", __dir__)' /app/bin/*

# Start the Rails server
EXPOSE 3000
CMD ["./bin/rails", "server"]
